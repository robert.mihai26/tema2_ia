from copy import deepcopy
import math

def addEdge(graph, n1, n2):
    if n1 in graph:
        # graph[n1].append(n2)
        graph[n1].add(n2)
    else:
        # graph[n1] = [n2]
        graph[n1] = {n2}

    if n2 in graph:
        # graph[n2].append(n1)
        graph[n2].add(n1)
    else:
        # graph[n2] = [n1]
        graph[n2] = {n1}

def removeEdge(graph, n1, n2):
    graph[n1].remove(n2)
    graph[n2].remove(n1)

def removeNode(graph, n):
    for neigh in list(graph[n]):
        removeEdge(graph, n, neigh)
    graph.pop(n)

def isEdge(graph, n1, n2):
    # sanity check
    if n1 in graph[n2] != n2 not in graph[n1]:
        print("MUCHIA ESTE ORIENTATA ! ! !")

    if n1 not in graph[n2]:
        return False
    return True

if __name__ == '__main__':
    file = open("bn1", "r")
    # print(file.read())
    lines = []
    with open("bn1", "r") as f:
        for line in f:
            lines.append(line.strip().split(' '))

    N = int(lines[0][0])
    M = int(lines[0][1])

    graph = {}
    for i in range(1, N+1):
        line = lines[i]
        # graph[line[0]] = {}
        pos = 2
        parents = []
        # 1.formare graf neorientat
        while line[pos] != ';':
            parents.append(line[pos])
            addEdge(graph, line[0], line[pos])
            pos += 1

        # 2.moralizare graf
        # print(line[0], parents)
        for p in range(0, len(parents) - 1):
            # print(graph[line[0]][p])
            p1 = parents[p]
            p2 = parents[p + 1]
            addEdge(graph, p1, p2)
            # print(line[0], p1, p2)
        # TODO probabilitati

    #TODO M interferente

    print(graph)

    # 3.graf cordal
    cordal = deepcopy(graph)
    desc = sorted(cordal.keys(), reverse=True)
    print("desc ", desc)
    while True:
        print(cordal)
        min_cost = math.inf
        best_node = None
        stop = True  # cand toate nodurile au cost 0, algoritmul se opreste
        for node in desc:
            if node not in cordal:
                continue
            print(node)
            neighbours = list(cordal[node])
            cost = 0
            for n1 in range(len(neighbours) - 1):
                for n2 in range(n1 + 1, len(neighbours)):
                    if not isEdge(cordal, neighbours[n1], neighbours[n2]):
                        stop = False
                        cost += 1
            print("NOD CURENT ", node, cost)
            if cost < min_cost:
                min_cost = cost
                best_node = node
        if stop:
            break
        print(min_cost, best_node, list(cordal[best_node]))
        if min_cost > 0:
            # adauga muchii
            neighbours = cordal[best_node]
            for n1 in neighbours:
                for n2 in neighbours:
                    if n1 == n2:
                        continue
                    if not isEdge(cordal, n1, n2):
                        addEdge(cordal, n1, n2)
        removeNode(cordal, best_node)
    print(cordal)
